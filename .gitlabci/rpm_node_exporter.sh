orig=$(pwd)
cd "${NE_NAME}" || exit 1
cp "../lib/systemd/${NE_NAME}.service" ~/rpmbuild/SOURCES/
cp "../packaging/rpm/${NE_NAME}.spec" ~/rpmbuild/SPECS/
cp "${NE_NAME}" "${HOME}/rpmbuild/BUILDROOT/${NE_NAME}"
cd ~/rpmbuild/SPECS || exit 1
rpmbuild -bb "${NE_NAME}.spec"
f=$(ls "$HOME/rpmbuild/RPMS/x86_64/")
cp "$HOME/rpmbuild/RPMS/x86_64/$f" "/builds/$CI_PROJECT_PATH/RPMs/"
rm -f "$HOME/rpmbuild/RPMS/x86_64/$f"
cd "$orig" || exit 1