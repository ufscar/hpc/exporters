targz="${NE_NAME}.tar.gz"
wget "${NE_URL}" -O "${targz}" --quiet
tar -xf "${targz}"
f=$(tar -tf "${targz}" | grep -E "^[^/]+/$")
mv "$f" "${NE_NAME}-${NE_VERSION}"
tar -czvf "${targz}" "${NE_NAME}-${NE_VERSION}"
mv "${NE_NAME}-${NE_VERSION}" "${NE_NAME}"
cp "${targz}" ~/rpmbuild/SOURCES/

cd "${NE_NAME}" || exit 1
echo "==================== make ==========================="
make
echo "==================== make ==========================="
cd ..