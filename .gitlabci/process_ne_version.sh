# Checks if specified version of Prometheus Slurm Exporter exists
OWNER_REPO="prometheus/node_exporter"
if [[ -n $NE_VERSION ]]; then
  NE_JSON=$(curl -s "https://api.github.com/repos/${OWNER_REPO}/releases/tags/${PSE_VERSION}")
  NE_URL=$(echo "$PSE_JSON" | jq -rc ".tarball_url")
fi;
if [[ -z $NE_URL ]]; then
  # If does not exist, use the latest
  NE_JSON=$(curl -s "https://api.github.com/repos/${OWNER_REPO}/releases/latest")
  NE_VERSION=$(echo "$NE_JSON" | jq -rc ".tag_name")
fi;
NE_JSON=$(curl -s "https://api.github.com/repos/${OWNER_REPO}/releases/tags/${NE_VERSION}")
NE_URL=$(echo "$NE_JSON" | jq -rc ".tarball_url")