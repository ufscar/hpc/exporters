%define _unpackaged_files_terminate_build 0
%define debug_package %{nil}
%define _name %{getenv:NE_NAME}
%define _version %{getenv:NE_VERSION}
%define _so %{getenv:SO}
%define _arch %{getenv:ARCHITECTURE}
%bcond_with sysvinit
%bcond_without systemd

Name:		%{_name}
Version:        %{_version}
%if %{with sysvinit}
Release:        1.sysvinit%{?dist}
%endif
%if %{with systemd}
Release:        1%{?dist}
%endif
Summary:	Prometheus exporter for machine metrics, written in Go with pluggable metric collectors.
Group:		System Environment/Daemons
License:	See the LICENSE file at github.
URL:		https://github.com/prometheus/node_exporter
Source0:        %{name}.tar.gz
Source1:        %{name}.service
Source2:        %{name}.sysconfig
Source3:        %{name}.init
BuildRoot:	%{_tmppath}/%{name}-%{version}-root
Requires(pre):  /usr/sbin/useradd
%if %{with sysvinit}
Requires:       daemonize
%endif
%if %{with systemd}
Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd
%endif
AutoReqProv:    No

%description

Prometheus exporter for machine metrics, written in Go with pluggable metric collectors.

%prep
%setup -q -n %{name}-%{version}

%build
echo

%install
mkdir -vp $RPM_BUILD_ROOT/var/run/prometheus
mkdir -vp $RPM_BUILD_ROOT/var/lib/prometheus
mkdir -vp $RPM_BUILD_ROOT/usr/bin
mkdir -vp $RPM_BUILD_ROOT/opt/prometheus
%if %{with sysvinit}
mkdir -vp $RPM_BUILD_ROOT/etc/init.d
mkdir -vp $RPM_BUILD_ROOT/etc/sysconfig
install -m 644 %{SOURCE2} $RPM_BUILD_ROOT/etc/sysconfig/%{name}
install -m 755 %{SOURCE3} $RPM_BUILD_ROOT/etc/init.d/%{name}
%endif
%if %{with systemd}
mkdir -vp $RPM_BUILD_ROOT/usr/lib/systemd/system
install -m 644 %{SOURCE1} $RPM_BUILD_ROOT/usr/lib/systemd/system/%{name}.service
%endif
install -m 755 /root/rpmbuild/BUILDROOT/%{name} $RPM_BUILD_ROOT/usr/bin/%{name}

%clean

%pre
getent group prometheus >/dev/null || groupadd -r prometheus
getent passwd prometheus >/dev/null || \
  useradd -r -g prometheus -s /sbin/nologin \
    -d /var/lib/prometheus -c "Prometheus exporter user" prometheus
exit 0

%files
%defattr(-,root,root,-)
/usr/bin/%{name}
/var/run/prometheus
/opt/prometheus
%if %{with sysvinit}
%config(noreplace) /etc/sysconfig/%{name}
/etc/init.d/%{name}
%endif
%if %{with systemd}
/usr/lib/systemd/system/%{name}.service
%endif