# Exporters

Repositório para geração dos RPMs dos exporters

## Variáveis de ambiente do CI/CD

- **PSE_VERSION**: Versão do [Prometheus Slurm Exporter](https://github.com/vpenso/prometheus-slurm-exporter). Se variável não existir ou seu conteúdo não representar uma versão válida, usa a mais recente.
- **NE_VERSION**: Versão do [Node Exporter](https://github.com/prometheus/node_exporter). Se variável não existir ou seu conteúdo não representar uma versão válida, usa a mais recente.
- **SO**: Sistema Operacional (linux, etc)
- **ARCHITECTURE**: Arquitetura (amd64, etc)

## Arquivos

- **lib/systemd/\*.service**: Arquivos service dos dois exporters
- **packaging/rpm/\*.spec**: Arquivos spec dos dois exporters